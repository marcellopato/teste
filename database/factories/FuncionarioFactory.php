<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Funcionario;
use App\Model;
use Faker\Generator as Faker;

$factory->define(Funcionario::class, function (Faker $faker) {
    return [
        'nome' => $faker->firstName,
        'sobrenome' => $faker->lastName,
        'email' => $faker->safeEmail,
        'idade' => $faker->numberBetween($min = 20, $max = 60), // https://stackoverflow.com/questions/43138197/laravel-faker-generate-gender-from-name
        'sexo' => $faker->randomElement(['male', 'female']),
        'password' => bcrypt('secret')
    ];
});