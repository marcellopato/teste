<?php

use App\Http\Controllers\Api\FuncionarioController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::namespace('API')->name('api.')->group(function () {
    Route::prefix('funcionarios')->group(function () {
        Route::get('/', 'FuncionarioController@index')->name('index.funcionarios');
        Route::get('/{id}', 'FuncionarioController@show')->name('show.fucionario');
        Route::post('/', 'FuncionarioController@store')->name('create.funcionario');
        Route::put('/{id}', 'FuncionarioController@update')->name('update.funcionario');
        Route::delete('/{id}', 'FuncionarioController@destroy')->name('delete.funcionario');
    });
    Route::get('/relatorio', 'FuncionarioController@relatorio')->name('relatorio.funcionario');
});