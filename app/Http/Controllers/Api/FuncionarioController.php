<?php

namespace App\Http\Controllers\Api;

use App\API\ApiError;
use App\Funcionario;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class FuncionarioController extends Controller
{
    /**
     * @var Funcionario;
     */
    private $funcionario;
    public function __construct(Funcionario $funcionario)
    {
        $this->funcionario = $funcionario;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json($this->funcionario->paginate(10));
    }

    /**
     * Show the report response.
     *
     * @return \Illuminate\Http\Response
     */
    public function relatorio()
    {
        $male = Funcionario::where('sexo', '=', 'male')->count();
        $female = Funcionario::where('sexo', '=', 'female')->count();
        $mediaIdade = Funcionario::all()->avg('idade');
        $maisNovo = Funcionario::all()->min('idade');
        $maisVelho = Funcionario::all()->max('idade');
        $data = [
            'Funcionários' => [
                'Sexo masculino' => $male,
                'Sexo feminino' => $female
            ],
            'Média de Idade' => [
                'idade' => round($mediaIdade, 0)
            ],
            'Funcionário mais novo' => [
                'idade' => $maisNovo
            ],
            'Funcionário mais velho' => [
                'idade' => $maisVelho
            ],
        ];
        return response()->json($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    // https://www.youtube.com/watch?v=oVRWQJE5a1c
    {
        try {
            $funcionarioData = $request->all();
            $this->funcionario->create($funcionarioData);
            $return = ['data' => ['msg' => 'Funcionário criado com sucesso!']];
            return response()->json($return, 201);
        } catch (\Exception $e) {
            if (config('app.debug')) {
                return response()->json(ApiError::errorMessage($e->getMessage(), 1010));
            }
            return response()->json(ApiError::errorMessage('Houve um erro ao adicionar um funcionário', 1010));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $funcionario = $this->funcionario->find($id);
        if (!$funcionario)
            return response()->json(['data' => ['msg', 'Funcionário não encontrado']], 404);
        $data = ['data' => $funcionario];
        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $funcionarioData = $request->all();
            $funcionario = $this->funcionario->find($id);
            $funcionario->update($funcionarioData);
            $return = ['data' => ['msg' => 'Funcionário atualizado com sucesso!']];
            return response()->json($return, 201);
        } catch (\Exception $e) {
            if (config('app.debug')) {
                return response()->json(ApiError::errorMessage($e->getMessage(), 1011));
            }
            return response()->json(ApiError::errorMessage('Houve um erro ao atualizar o funcionário', 1011));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Funcionario $id)
    {
        try {
            $id->delete();
            return response()->json(['data', ['msg' => 'Funcionário: ' . $id->nome . ' ' .  $id->sobrenome . ' removido com sucesso'], 200]);
        } catch (\Exception $e) {
            if (config('app.debug')) {
                return response()->json(ApiError::errorMessage($e->getMessage(), 1012));
            }
            return response()->json(ApiError::errorMessage('Houve um erro ao remover o funcionário', 1012));
        }
    }
}