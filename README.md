## Sobre esse teste

- Clone esse repositório em seu computador.
- Use o composer para gerar a key que é necessária no arquivo de configuração (.env).
- Crie um banco de dados MySQL e configure seua credenciais no arquivo de configuração (.env).
- Faça ```php artisan migrate``` para criar o banco de dados.
- Faça ```php artisan db:seed``` para alimentar o banco com dados de exemplo.


## Ending points

- Listar todos usando o verbo GET: [funcionarios](http://127.0.0.1:8000/api/funcionarios)
- Listar um funcionário usando o verbo GET: [funcionário](http://127.0.0.1:8000/api/funcionario/{id})
- Criar um funcionário usando o verbo POST: [criar](http://127.0.0.1:8000/api/funcionarios)
    body: nome, sobrenome, email, idade, sexo, password
- Atualizar um funcionário usando o verbo PUT: [atualizar](http://127.0.0.1:8000/api/funcionario/{id})
- Apagar usando o verbo DELETE: [apagar](http://127.0.0.1:8000/api/funcionario/{id})
- Para ver o relatório com o verbo GET: [relatorio](http://127.0.0.1:8000/api/relatorio)

## Licensa

O conteúdo desse repositório é apenas para fins coorporativos e são dados fictícios.
